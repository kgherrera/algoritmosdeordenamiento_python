'''
Created on 12/11/2021

@author: Herrera
'''
import time
import random
import math

class AlgoritmosOrdenamiento:
                    
    @staticmethod
    def burbuja1(arreglo):
        for i in range(1, len(arreglo)):
            for j in range(0, len(arreglo)-i):
                if arreglo[j]>arreglo[j+1]:
                    aux = arreglo[j]
                    arreglo[j] = arreglo[j+1]
                    arreglo[j+1] = aux
        

    
    @staticmethod
    def burbuja2(arreglo):
        i = 0
        inicio = time.time()
        while i<len(arreglo):
            j=i
            while j<len(arreglo):
                if arreglo[i]>arreglo[j]:
                    aux = arreglo[i]
                   
                    arreglo[i] = arreglo[j]
                    arreglo[j] = aux
                    
                j+=1
            i+=1 
    
    @staticmethod
    def burbuja3(numeros): 
        i = 1
        
        while(i < len(numeros)):
            for j in range(len(numeros)-i):
                if(numeros[j] > numeros[j+1]):
                    aux = numeros[j]
                    numeros[j] = numeros[j+1]
                    numeros[j+1] = aux
            i = 1 + i  
    
    @staticmethod
    def intercambiar(a, i, j):
        aux = a[i]
        a[i] = a[j]
        a[j] = aux
        
    @staticmethod
    def quicksort(a, primero, ultimo):
        central = int((primero + ultimo)/2)
        pivote = a[central]
        i = primero
        j = ultimo
        
        while(i <= j):
            while(a[i] < pivote):
                i+=1
            while(a[j] > pivote):
                j-=1
            if(i <= j):
                AlgoritmosOrdenamiento.intercambiar(a, i, j)
                i+=1
                j-=1
            if(primero < j):
                AlgoritmosOrdenamiento.quicksort(a, primero, j)
            if(i < ultimo):
                AlgoritmosOrdenamiento.quicksort(a, i, ultimo)
                
    @staticmethod
    def quicksortLlamada(a):
        
        inicio = time.time()
        AlgoritmosOrdenamiento.quicksort(a, 0, len(a)-1)
        fin = time.time()
        print(f"Tiempo de ejecucion: {fin - inicio}")
        
    # ====== Ordenamiento de objetos ========
        
    @staticmethod
    def quicksortObjetos(a, primero, ultimo):
        central = int((primero + ultimo)/2)
        pivote = a[central]
        i = primero
        j = ultimo
        
        while(i <= j):
            while(a[i].promedio < pivote.promedio):
                i+=1
            while(a[j].promedio > pivote.promedio):
                j-=1
            if(i <= j):
                AlgoritmosOrdenamiento.intercambiar(a, i, j)
                i+=1
                j-=1
            if(primero < j):
                AlgoritmosOrdenamiento.quicksortObjetos(a, primero, j)
            if(i < ultimo):
                AlgoritmosOrdenamiento.quicksortObjetos(a, i, ultimo)
                
    @staticmethod
    def quicksortLlamadaObjeto(a):
        inicio = time.time()
        AlgoritmosOrdenamiento.quicksortObjetos(a, 0, len(a)-1)
        fin = time.time()
        print(f"Tiempo de ejecucion: {fin - inicio}")
        
    
    # ====== Shellsort ========
    
    @staticmethod
    def shellSort(numeros):
        intervalo = int(len(numeros)/2)
        while(intervalo>0):
            for i in range(int(intervalo), len(numeros)):
                j=i-int(intervalo)
                while(j>=0):
                    k=j+int(intervalo)
                    if numeros[j]<=numeros[k]:
                        j-=1
                    else:
                        aux=numeros[j]
                        numeros[j]=numeros[k]
                        numeros[k]=aux
                        j-=int(intervalo)
            intervalo=int(intervalo)/2
    
    # ====== radix ========
    

    def counting_sort(self, A, digit, radix):
        
        B = [0]* len(A)
        C = [0]* int(radix)
        
        for i in range(0, len(A)):
            digit_of_Ai = (A[i]/radix ** digit)% radix
            C[int(digit_of_Ai)] = C[int(digit_of_Ai)] +1 
            
        for j in range(1, radix):
            C[j]= C[j]+ C[j-1]
            
        for m in range(len(A)-1, -1, -1):
            digit_of_Ai = (A[m]/radix ** digit)% radix
            C[int(digit_of_Ai)]= C[int(digit_of_Ai)]-1
            B[C[int(digit_of_Ai)]] = A[m]
        return B 
    
    def radix_sort(self, A, radix):
        
        k = max(A)
        
        output = A 
        
        digits = int(math.floor(math.log(k, radix)+1))
        for i in range(digits):
            output= self.counting_sort(output, i, radix)
            
        return output



class Alumno:
    def __init__(self, nombre, carrera, promedio):
        self.nombre = nombre
        self.carrera = carrera
        self.promedio = promedio
        
    def __str__(self):
        return f"Alumno [nombre = {self.nombre} carrera = {self.carrera} promedio = {self.promedio} ]"


vect = []
opcion = 0
a1 = AlgoritmosOrdenamiento()
for i in range(10):
    vect.append(random.randrange(70, 99, 2))

while(opcion != 20):
    print("\nIntroduce metodo de ordenamiento: ");
    print("1) burbuja 1");
    print("2) burbuja 2");
    print("3) burbuja 3");
    print("4) Quicksort");
    print("5) Shellsort");
    print("6) Radix")
    print("7) Salir");
    opcion = int(input("Introduce opcion: "))
    
    vector = vect.copy()
    
    
    if (opcion == 1):
        print("\nVector sin ordenar: ")
        print(vector)
        AlgoritmosOrdenamiento.burbuja1(vector)
        print("\nVector ordenado: ")
        print(vector)
        
    if (opcion == 2):
        print("\nVector sin ordenar: ")
        print(vector)
        AlgoritmosOrdenamiento.burbuja2(vector)
        print("\nVector ordenado: ")
        print(vector)
        
    if (opcion == 3):
        print("\nVector sin ordenar: ")
        print(vector)
        AlgoritmosOrdenamiento.burbuja3(vector)
        print("\nVector ordenado: ")
        print(vector)
    
    if (opcion == 4):
        print("\nVector sin ordenar: ")
        print(vector)
        AlgoritmosOrdenamiento.quicksortLlamada(vector)
        print("\nVector ordenado: ")
        print(vector)
        
    if (opcion == 5):
        print("\nVector sin ordenar: ")
        print(vector)
        AlgoritmosOrdenamiento.shellSort(vector)
        print("\nVector ordenado: ")
        print(vector)
        
    if (opcion == 6):
        print("\nVector sin ordenar: ")
        print(vector)
        
        print("\nVector ordenado: ")
        print(a1.radix_sort(vector, 10))
        
    if (opcion == 7):
        print("\nSaliendo . . .")
    

'''
cadenas = ["Paco","Luis","Pablo"]
alumnos = [Alumno("Pablo", "ISC", 90), Alumno("Luis", "ISC", 89), Alumno("Paco", "ADM", 19)]

for i in range(10):
    vector.append(random.randrange(70, 101, 2))
    
print(vector)

OrdenamientoBurbuja.burbuja3(vector)
print(vector)
print("Otra prueba")

AlgoritmosOrdenamiento.quicksortLlamada(vector)
print(vector)

print()

print(cadenas)
AlgoritmosOrdenamiento.quicksortLlamada(cadenas)
print(cadenas)

print()

for i in alumnos:
    print(f"{i}, ", end="")
print()

AlgoritmosOrdenamiento.quicksortLlamadaObjeto(alumnos)
for i in alumnos:
    print(f"{i}, ", end="")
    
print()

'''





